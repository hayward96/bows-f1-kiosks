package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SuccessResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-17T10:02:11.711Z[GMT]")
public class SuccessResponse   {
  @JsonProperty("successCode")
  private Integer successCode = null;

  @JsonProperty("successMessage")
  private String successMessage = null;

  public SuccessResponse successCode(Integer successCode) {
    this.successCode = successCode;
    return this;
  }

  /**
   * HTTP Status Code
   * @return successCode
  **/
  @ApiModelProperty(example = "200", value = "HTTP Status Code")
  
    public Integer getSuccessCode() {
    return successCode;
  }

  public void setSuccessCode(Integer successCode) {
    this.successCode = successCode;
  }

  public SuccessResponse successMessage(String successMessage) {
    this.successMessage = successMessage;
    return this;
  }

  /**
   * Message for the success
   * @return successMessage
  **/
  @ApiModelProperty(example = "Successfully found on system", value = "Message for the success")
  
    public String getSuccessMessage() {
    return successMessage;
  }

  public void setSuccessMessage(String successMessage) {
    this.successMessage = successMessage;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SuccessResponse successResponse = (SuccessResponse) o;
    return Objects.equals(this.successCode, successResponse.successCode) &&
        Objects.equals(this.successMessage, successResponse.successMessage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(successCode, successMessage);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SuccessResponse {\n");
    
    sb.append("    successCode: ").append(toIndentedString(successCode)).append("\n");
    sb.append("    successMessage: ").append(toIndentedString(successMessage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
