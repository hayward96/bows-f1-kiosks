package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * LoginRequest
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-17T10:02:11.711Z[GMT]")
public class LoginRequest   {
  @JsonProperty("cardID")
  private String cardID = null;

  @JsonProperty("pin")
  private String pin = null;

  public LoginRequest cardID(String cardID) {
    this.cardID = cardID;
    return this;
  }

  /**
   * Unique sequence of 16 alphanumeric characters
   * @return cardID
  **/
  @ApiModelProperty(example = "r7jTG7dqBy5wGO4L", value = "Unique sequence of 16 alphanumeric characters")
  
  @Size(min=16,max=16)   public String getCardID() {
    return cardID;
  }

  public void setCardID(String cardID) {
    this.cardID = cardID;
  }

  public LoginRequest pin(String pin) {
    this.pin = pin;
    return this;
  }

  /**
   * 4 digit pin number to access system
   * @return pin
  **/
  @ApiModelProperty(example = "1234", value = "4 digit pin number to access system")
  
  @Size(min=4,max=4)   public String getPin() {
    return pin;
  }

  public void setPin(String pin) {
    this.pin = pin;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LoginRequest loginRequest = (LoginRequest) o;
    return Objects.equals(this.cardID, loginRequest.cardID) &&
        Objects.equals(this.pin, loginRequest.pin);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cardID, pin);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LoginRequest {\n");
    
    sb.append("    cardID: ").append(toIndentedString(cardID)).append("\n");
    sb.append("    pin: ").append(toIndentedString(pin)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
