package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * LogoutRequest
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-24T12:55:54.012Z[GMT]")
public class LogoutRequest   {
  @JsonProperty("cardID")
  private String cardID = null;

  public LogoutRequest cardID(String cardID) {
    this.cardID = cardID;
    return this;
  }

  /**
   * Unique sequence of 16 alphanumeric characters
   * @return cardID
  **/
  @ApiModelProperty(example = "r7jTG7dqBy5wGO4L", value = "Unique sequence of 16 alphanumeric characters")
  
  @Size(min=16,max=16)   public String getCardID() {
    return cardID;
  }

  public void setCardID(String cardID) {
    this.cardID = cardID;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LogoutRequest logoutRequest = (LogoutRequest) o;
    return Objects.equals(this.cardID, logoutRequest.cardID);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cardID);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LogoutRequest {\n");
    
    sb.append("    cardID: ").append(toIndentedString(cardID)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


