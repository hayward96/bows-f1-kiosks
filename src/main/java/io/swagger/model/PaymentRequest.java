package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PaymentRequest
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-23T17:17:27.651Z[GMT]")
public class PaymentRequest   {
  @JsonProperty("paymentValue")
  private Double paymentValue = null;

  @JsonProperty("cardID")
  private String cardID = null;

  public PaymentRequest paymentValue(Double paymentValue) {
    this.paymentValue = paymentValue;
    return this;
  }

  /**
   * Payment that is made by the card
   * @return paymentValue
  **/
  @ApiModelProperty(example = "10.25", value = "Payment that is made by the card")
  
    @Valid
    public Double getPaymentValue() {
    return paymentValue;
  }

  public void setPaymentValue(Double paymentValue) {
    this.paymentValue = paymentValue;
  }

  public PaymentRequest cardID(String cardID) {
    this.cardID = cardID;
    return this;
  }

  /**
   * Unique sequence of 16 alphanumeric characters
   * @return cardID
  **/
  @ApiModelProperty(example = "r7jTG7dqBy5wGO4L", value = "Unique sequence of 16 alphanumeric characters")
  
  @Size(min=16,max=16)   public String getCardID() {
    return cardID;
  }

  public void setCardID(String cardID) {
    this.cardID = cardID;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentRequest paymentRequest = (PaymentRequest) o;
    return Objects.equals(this.paymentValue, paymentRequest.paymentValue) &&
        Objects.equals(this.cardID, paymentRequest.cardID);
  }

  @Override
  public int hashCode() {
    return Objects.hash(paymentValue, cardID);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentRequest {\n");
    
    sb.append("    paymentValue: ").append(toIndentedString(paymentValue)).append("\n");
    sb.append("    cardID: ").append(toIndentedString(cardID)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}