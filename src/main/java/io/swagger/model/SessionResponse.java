package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SessionResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-24T09:52:06.621Z[GMT]")
public class SessionResponse   {
  @JsonProperty("message")
  private String message = null;

  @JsonProperty("fullName")
  private String fullName = null;

  public SessionResponse message(String message) {
    this.message = message;
    return this;
  }

  /**
   * Message to display
   * @return message
  **/
  @ApiModelProperty(example = "Hello", value = "Message to display")
  
    public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public SessionResponse fullName(String fullName) {
    this.fullName = fullName;
    return this;
  }

  /**
   * Full name of employee
   * @return fullName
  **/
  @ApiModelProperty(example = "Daniel Hayward", value = "Full name of employee")
  
    public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SessionResponse sessionResponse = (SessionResponse) o;
    return Objects.equals(this.message, sessionResponse.message) &&
        Objects.equals(this.fullName, sessionResponse.fullName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(message, fullName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SessionResponse {\n");
    
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    fullName: ").append(toIndentedString(fullName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
