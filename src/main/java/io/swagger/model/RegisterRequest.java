package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * RegisterRequest
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-17T10:02:11.711Z[GMT]")
public class RegisterRequest   {
  @JsonProperty("cardID")
  private String cardID = null;

  @JsonProperty("employeeID")
  private String employeeID = null;

  @JsonProperty("firstName")
  private String firstName = null;

  @JsonProperty("lastName")
  private String lastName = null;

  @JsonProperty("email")
  private String email = null;

  @JsonProperty("phone")
  private String phone = null;

  @JsonProperty("pin")
  private String pin = null;

  public RegisterRequest cardID(String cardID) {
    this.cardID = cardID;
    return this;
  }

  /**
   * Unique sequence of 16 alphanumeric characters
   * @return cardID
  **/
  @ApiModelProperty(example = "r7jTG7dqBy5wGO4L", value = "Unique sequence of 16 alphanumeric characters")
  
  @Size(min=16,max=16)   public String getCardID() {
    return cardID;
  }

  public void setCardID(String cardID) {
    this.cardID = cardID;
  }

  public RegisterRequest employeeID(String employeeID) {
    this.employeeID = employeeID;
    return this;
  }

  /**
   * Unique alphanumeric ID of employee
   * @return employeeID
  **/
  @ApiModelProperty(example = "qWe123Rty5Pz", value = "Unique alphanumeric ID of employee")
  
    public String getEmployeeID() {
    return employeeID;
  }

  public void setEmployeeID(String employeeID) {
    this.employeeID = employeeID;
  }

  public RegisterRequest firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

  /**
   * First name of the employee
   * @return firstName
  **/
  @ApiModelProperty(example = "Daniel", value = "First name of the employee")
  
    public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public RegisterRequest lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  /**
   * Last name of the employee
   * @return lastName
  **/
  @ApiModelProperty(example = "Hayward", value = "Last name of the employee")
  
    public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public RegisterRequest email(String email) {
    this.email = email;
    return this;
  }

  /**
   * Email of the employee
   * @return email
  **/
  @ApiModelProperty(example = "daniel.hayward@colt.net", value = "Email of the employee")
  
    public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public RegisterRequest phone(String phone) {
    this.phone = phone;
    return this;
  }

  /**
   * Mobile number of the employee
   * @return phone
  **/
  @ApiModelProperty(example = "7123456789", value = "Mobile number of the employee")
  
  @Size(min=11,max=11)   public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public RegisterRequest pin(String pin) {
    this.pin = pin;
    return this;
  }

  /**
   * 4 digit pin number to access system
   * @return pin
  **/
  @ApiModelProperty(example = "1234", value = "4 digit pin number to access system")
  
  @Size(min=4,max=4)   public String getPin() {
    return pin;
  }

  public void setPin(String pin) {
    this.pin = pin;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RegisterRequest registerRequest = (RegisterRequest) o;
    return Objects.equals(this.cardID, registerRequest.cardID) &&
        Objects.equals(this.employeeID, registerRequest.employeeID) &&
        Objects.equals(this.firstName, registerRequest.firstName) &&
        Objects.equals(this.lastName, registerRequest.lastName) &&
        Objects.equals(this.email, registerRequest.email) &&
        Objects.equals(this.phone, registerRequest.phone) &&
        Objects.equals(this.pin, registerRequest.pin);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cardID, employeeID, firstName, lastName, email, phone, pin);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RegisterRequest {\n");
    
    sb.append("    cardID: ").append(toIndentedString(cardID)).append("\n");
    sb.append("    employeeID: ").append(toIndentedString(employeeID)).append("\n");
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("    pin: ").append(toIndentedString(pin)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
