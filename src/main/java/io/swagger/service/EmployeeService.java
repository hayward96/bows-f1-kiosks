package io.swagger.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
	
    private FcsMock fcService;
    
    @Autowired
    public EmployeeService(FcsMock fcService) {
        this.fcService = fcService;
    }
 
    public String fcID(String employeeID, String firstName, String lastName, String mobile, String email) {
        return fcService.fcID(employeeID, firstName, lastName, mobile, email);
    }
    
    public Double payment(Double payment, String fcID) {
    	return fcService.payment(payment, fcID);
    }

}
