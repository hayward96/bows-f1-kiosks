package io.swagger.service;

import org.springframework.stereotype.Service;

@Service
public class FcsMock {

	public String fcID(String employeeID, String firstName, String lastName, String mobile, String email) {
		if (employeeID.equals("asXZ67op08Y")) {
			return "RTYIZ12LJ99";
		}
		return null;
	};

	public Double payment(Double pay, String fcID) {
		double topup = 10;
		double payment = -5.00;
		if (pay == topup) {
			return 20 + topup;
		} else if (pay == payment) {
			return 20 + payment;
		}
		return null;

	};

}
