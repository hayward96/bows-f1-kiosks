package io.swagger.api;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.context.request.RequestContextHolder;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiParam;
import io.swagger.database.EmployeeTable;
import io.swagger.database.SessionTable;
import io.swagger.model.ErrorResponse;
import io.swagger.model.RegisterRequest;
import io.swagger.model.SessionResponse;
import io.swagger.model.SuccessResponse;
import io.swagger.service.EmployeeService;
import io.swagger.service.FcsMock;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-17T10:02:11.711Z[GMT]")
@Controller
public class RegisterApiController implements RegisterApi {

	private static final Logger log = LoggerFactory.getLogger(RegisterApiController.class);

	private final ObjectMapper objectMapper;

	private final HttpServletRequest request;

	@org.springframework.beans.factory.annotation.Autowired
	public RegisterApiController(ObjectMapper objectMapper, HttpServletRequest request) {
		this.objectMapper = objectMapper;
		this.request = request;
	}

	public ResponseEntity registerCardIDGet(
			@Size(min = 16, max = 16) @ApiParam(value = "", required = true) @PathVariable("cardID") String cardID) {
		String accept = request.getHeader("Accept");
		if (accept != null && accept.contains("/")) {
			try {
				if (cardID != null) {
					EmployeeTable et = new EmployeeTable();
					String name = et.employeeCheck(cardID);

					if (name != null) {

						SuccessResponse sr = new SuccessResponse();
						sr.setSuccessCode(200);
						sr.setSuccessMessage("Employee found please enter pin");
						String response = objectMapper.writeValueAsString(sr);
						return new ResponseEntity<SuccessResponse>(
								objectMapper.readValue(response, SuccessResponse.class), HttpStatus.OK);
					} else {

						ErrorResponse er = new ErrorResponse();
						er.setErrorCode(404);
						er.setErrorMessage("Employee name not found on system");
						String response = objectMapper.writeValueAsString(er);
						return new ResponseEntity<ErrorResponse>(objectMapper.readValue(response, ErrorResponse.class),
								HttpStatus.OK);
					}

				} else {

					ErrorResponse er = new ErrorResponse();
					er.setErrorCode(404);
					er.setErrorMessage("CardID not found on system");
					String response = objectMapper.writeValueAsString(er);
					return new ResponseEntity<ErrorResponse>(objectMapper.readValue(response, ErrorResponse.class),
							HttpStatus.OK);
				}

			} catch (IOException e) {
				log.error("Couldn't serialize response for content type application/json", e);
				return new ResponseEntity<SuccessResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		return new ResponseEntity<SuccessResponse>(HttpStatus.NOT_IMPLEMENTED);
	}

	public ResponseEntity registerPost(
			@ApiParam(value = "", required = true) @Valid @RequestBody RegisterRequest body) {

		String sessionID = RequestContextHolder.currentRequestAttributes().getSessionId();
		String accept = request.getHeader("Content-Type");

		if (accept != null && accept.contains("application/json")) {
			try {
				SessionTable session = new SessionTable();
				EmployeeTable et = new EmployeeTable();
				FcsMock fcs = new FcsMock();
				EmployeeService us = new EmployeeService(fcs);

				String fcID = us.fcID(body.getEmployeeID(), body.getFirstName(), body.getLastName(), body.getPhone(),
						body.getEmail());
				if (fcID != null) {
					// create user
					et.row(body, fcID);
					// create session for card
					boolean sessionCreate = session.row(body.getCardID(), sessionID);
					if (sessionCreate == true) {
						SessionResponse sr = new SessionResponse();
						sr.setMessage("Welcome " + body.getFirstName() + " " + body.getLastName());
						sr.setFullName(body.getFirstName() + " " + body.getLastName());
						String response = objectMapper.writeValueAsString(sr);
						return new ResponseEntity<SessionResponse>(
								objectMapper.readValue(response, SessionResponse.class), HttpStatus.OK);
					} else {
						ErrorResponse er = new ErrorResponse();
						er.setErrorCode(500);
						er.setErrorMessage("Session error.");
						String errorResp = objectMapper.writeValueAsString(er);
						return new ResponseEntity<ErrorResponse>(objectMapper.readValue(errorResp, ErrorResponse.class),
								HttpStatus.INTERNAL_SERVER_ERROR);
					}
				} else {
					ErrorResponse er = new ErrorResponse();
					er.setErrorCode(404);
					er.setErrorMessage("User not found on the first catering system.");
					String errorResp = objectMapper.writeValueAsString(er);
					return new ResponseEntity<ErrorResponse>(objectMapper.readValue(errorResp, ErrorResponse.class),
							HttpStatus.NOT_FOUND);
				}
			} catch (IOException e) {
				log.error("Couldn't serialize response for content type application/json", e);
				return new ResponseEntity<SessionResponse>(HttpStatus.BAD_REQUEST);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return new ResponseEntity<SessionResponse>(HttpStatus.NOT_IMPLEMENTED);
	}

}
