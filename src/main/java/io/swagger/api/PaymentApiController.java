package io.swagger.api;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiParam;
import io.swagger.database.EmployeeTable;
import io.swagger.model.ErrorResponse;
import io.swagger.model.PaymentRequest;
import io.swagger.model.PaymentResponse;
import io.swagger.service.EmployeeService;
import io.swagger.service.FcsMock;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-23T17:17:27.651Z[GMT]")
@Controller
public class PaymentApiController implements PaymentApi {

	private static final Logger log = LoggerFactory.getLogger(PaymentApiController.class);

	private final ObjectMapper objectMapper;

	private final HttpServletRequest request;

	@org.springframework.beans.factory.annotation.Autowired
	public PaymentApiController(ObjectMapper objectMapper, HttpServletRequest request) {
		this.objectMapper = objectMapper;
		this.request = request;
	}

	public ResponseEntity paymentPut(
			@ApiParam(value = "Add or subtract from balance", required = true) @Valid @RequestBody PaymentRequest body) {
		String accept = request.getHeader("Content-Type");
		if (accept != null && accept.contains("application/json")) {
			try {
				EmployeeTable et = new EmployeeTable();
				String fcID = et.fcUnique(body.getCardID());

				FcsMock fcs = new FcsMock();
				EmployeeService es = new EmployeeService(fcs);
				Double newBal = es.payment(body.getPaymentValue(), fcID);
				if (newBal != null) {
					// return new_bal and success
					PaymentResponse pr = new PaymentResponse();
					pr.setBalance(newBal);
					String balResp = objectMapper.writeValueAsString(pr);
					return new ResponseEntity<PaymentResponse>(objectMapper.readValue(balResp, PaymentResponse.class),
							HttpStatus.OK);
				} else {
					ErrorResponse er = new ErrorResponse();
					er.setErrorCode(424);
					er.setErrorMessage("Failed to make payment, please contact first catering.");
					String errorResp = objectMapper.writeValueAsString(er);
					return new ResponseEntity<ErrorResponse>(objectMapper.readValue(errorResp, ErrorResponse.class),
							HttpStatus.FAILED_DEPENDENCY);
					// return error, transaction not complete
				}

			} catch (IOException e) {
				log.error("Couldn't serialize response for content type application/json", e);
				return new ResponseEntity<PaymentResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		return new ResponseEntity<PaymentResponse>(HttpStatus.NOT_IMPLEMENTED);
	}

}