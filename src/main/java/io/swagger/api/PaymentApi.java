/**
 * NOTE: This class is auto generated by the swagger code generator program (3.0.18).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package io.swagger.api;

import io.swagger.model.ErrorResponse;
import io.swagger.model.PaymentRequest;
import io.swagger.model.PaymentResponse;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.CookieValue;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;
import java.util.Map;
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-23T17:17:27.651Z[GMT]")
@Api(value = "payment", description = "the payment API")
public interface PaymentApi {

    @ApiOperation(value = "Transactions on employee card", nickname = "paymentPut", notes = "Transactions that can only be done by the logged in employee", response = PaymentResponse.class, tags={ "Payment", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Successfully completed transaction", response = PaymentResponse.class),
        @ApiResponse(code = 402, message = "Not enough funds on card, top up required", response = ErrorResponse.class),
        @ApiResponse(code = 403, message = "Cannot complete the transaction", response = ErrorResponse.class) })
    @RequestMapping(value = "/payment",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.PUT)
    ResponseEntity<PaymentResponse> paymentPut(@ApiParam(value = "Add or subtract from balance" ,required=true )  @Valid @RequestBody PaymentRequest body
);

}
