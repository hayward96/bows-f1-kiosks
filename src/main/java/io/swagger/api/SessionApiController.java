package io.swagger.api;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.context.request.RequestContextHolder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiParam;
import io.swagger.database.EmployeeTable;
import io.swagger.database.SessionTable;
import io.swagger.model.ErrorResponse;
import io.swagger.model.LoginRequest;
import io.swagger.model.LogoutRequest;
import io.swagger.model.SessionResponse;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-17T10:02:11.711Z[GMT]")
@Controller
public class SessionApiController implements SessionApi {

	private static final Logger log = LoggerFactory.getLogger(SessionApiController.class);

	private final ObjectMapper objectMapper;

	private final HttpServletRequest request;

	@org.springframework.beans.factory.annotation.Autowired
	public SessionApiController(ObjectMapper objectMapper, HttpServletRequest request) {
		this.objectMapper = objectMapper;
		this.request = request;
	}

	public ResponseEntity logoutUser(@ApiParam(value = "", required = true) @Valid @RequestBody LogoutRequest body) {
		String accept = request.getHeader("Content-Type");
		if (accept != null && accept.contains("application/json")) {
			String sessionID = RequestContextHolder.currentRequestAttributes().getSessionId();
			String cardID = body.getCardID();
			try {
				EmployeeTable employee = new EmployeeTable();
				SessionTable session = new SessionTable();
				String name = employee.employeeCheck((body.getCardID()));

				SessionResponse sr = new SessionResponse();

				if (cardID != null) {
					session.deleteSession(sessionID);
					sr.setMessage("Goodbye " + name);
					sr.setFullName(name);
					String seshResp = objectMapper.writeValueAsString(sr);
					System.out.println(seshResp);
					employee.closeConection();

					return new ResponseEntity<SessionResponse>(objectMapper.readValue(seshResp, SessionResponse.class),
							HttpStatus.OK);
				}
			} catch (Exception e) {
				log.error("Couldn't serialize response for content type application/json", e);
				return new ResponseEntity<SessionResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		return new ResponseEntity<SessionResponse>(HttpStatus.NOT_IMPLEMENTED);
	}

	public ResponseEntity sessionLoginPost(
			@ApiParam(value = "", required = true) @Valid @RequestBody LoginRequest body) {
		String accept = request.getHeader("Content-Type");
		if (accept != null && accept.contains("application/json")) {
			try {
				String sessionID = RequestContextHolder.currentRequestAttributes().getSessionId();
				String cardID = body.getCardID();
				try {
					EmployeeTable employee = new EmployeeTable();
					SessionTable session = new SessionTable();
					String name = employee.employeeCheck((body.getCardID()));
					String fcID = employee.validatePin(body.getPin(), body.getCardID());

					if (fcID != null) {
						boolean sessionStart = session.userSesssion(cardID, sessionID);

						SessionResponse sr = new SessionResponse();

						// if no existing session with card details
						if (sessionStart == false) {
							// if there is a session that was valid send a goodbye message and delete
							// session
							sr.setMessage("Goodbye " + name);
							sr.setFullName(name);
						} else {
							// create session
							sr.setMessage("Welcome " + name);
							sr.setFullName(name);

						}

						String seshResp = objectMapper.writeValueAsString(sr);
						System.out.println(seshResp);
						employee.closeConection();
						return new ResponseEntity<SessionResponse>(
								objectMapper.readValue(seshResp, SessionResponse.class), HttpStatus.OK);
					} else {
						session.deleteSession(sessionID);
						ErrorResponse er = new ErrorResponse();
						er.setErrorCode(401);
						er.setErrorMessage("Invalid Pin");
						String errorResp = objectMapper.writeValueAsString(er);
						System.out.println(errorResp);
						employee.closeConection();
						return new ResponseEntity<ErrorResponse>(objectMapper.readValue(errorResp, ErrorResponse.class),
								HttpStatus.UNAUTHORIZED);
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (Exception e) {
				log.error("Couldn't serialize response for content type application/json", e);
				return new ResponseEntity<SessionResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		return new ResponseEntity<SessionResponse>(HttpStatus.NOT_FOUND);
	}

}
