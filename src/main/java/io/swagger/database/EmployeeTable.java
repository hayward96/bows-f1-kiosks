package io.swagger.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import io.swagger.model.RegisterRequest;

public class EmployeeTable extends DatabaseConnection {

	// register new card
	public void row(RegisterRequest register, String fcUniqueID) throws ClassNotFoundException, SQLException {
		String query = "INSERT INTO Employee (cardID, employeeID, firstName, lastName, pin, fcUniqueID) VALUES (?, ?, ?, ?, ?, ?)";
		PreparedStatement preparedStmt = connection().prepareStatement(query);
		preparedStmt.setString(1, register.getCardID());
		preparedStmt.setString(2, register.getEmployeeID());
		preparedStmt.setString(3, register.getFirstName());
		preparedStmt.setString(4, register.getLastName());
		preparedStmt.setString(5, register.getPin());
		preparedStmt.setString(6, fcUniqueID);
		entry(preparedStmt);

	}

	// Check if card exists in table and return their first and last name if they do
	public String employeeCheck(String cardID) {
		String query = "SELECT firstName, lastName FROM Employee WHERE cardID = ?";
		try {
			PreparedStatement preparedStmt = connection().prepareStatement(query);
			preparedStmt.setString(1, cardID);
			ResultSet rs = preparedStmt.executeQuery();
			while (rs.next()) {
				String firstName = rs.getString("firstName");
				String lastName = rs.getString("lastName");
				return firstName + " " + lastName;
			}

			return null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	//Get fc unqiue id 
	public String fcUnique(String cardID) {
		String query = "SELECT fcUniqueID FROM Employee WHERE cardID = ?";
		try {
			PreparedStatement preparedStmt = connection().prepareStatement(query);
			preparedStmt.setString(1, cardID);
			ResultSet rs = preparedStmt.executeQuery();
			while (rs.next()) {
				String fcUniqueID = rs.getString("fcUniqueID");
				return fcUniqueID;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return null;
	}

	// Validate the entry of the pin
	public String validatePin(String pin, String cardID) {
		String query = "SELECT pin, fcUniqueID FROM Employee WHERE cardID = ?";
		try {
			PreparedStatement preparedStmt = connection().prepareStatement(query);
			preparedStmt.setString(1, cardID);
			ResultSet rs = preparedStmt.executeQuery();
			while (rs.next()) {
				String pinOnDB = rs.getString("pin");
				String fcUniqueID = rs.getString("fcUniqueID");
				// if the pin matches the pin on the db return the first catering unique ID
				if (pinOnDB.equals(pin)) {
					return fcUniqueID;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return null;
	}

}
