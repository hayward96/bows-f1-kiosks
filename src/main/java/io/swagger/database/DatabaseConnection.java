package io.swagger.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class DatabaseConnection {
	
	Connection con = null;

	public Connection connection() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bowsdb?autoReconnect=true", "root", "Spaniel96");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}

	public void closeConection() {
		try {
			connection().close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void entry(PreparedStatement preparedStmt) {
		try {
			preparedStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				preparedStmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
