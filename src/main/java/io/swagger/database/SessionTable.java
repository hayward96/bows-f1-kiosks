package io.swagger.database;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class SessionTable extends DatabaseConnection {

	public boolean row(String cardID, String sessionID) throws SQLException {
		String query = "INSERT INTO Session (sessionID, cardID, sessionTime) VALUES (?,?,?)";
		PreparedStatement preparedStmt = connection().prepareStatement(query);
		preparedStmt.setString(1, sessionID);
		preparedStmt.setString(2, cardID);
		// Time stamp in 10 mins
		Timestamp sessionTime = new Timestamp(new Date(System.currentTimeMillis() + 10 * 60 * 1000).getTime());
		preparedStmt.setTimestamp(3, sessionTime);
		entry(preparedStmt);
		return true;
	}

	public boolean validSession(String cardID, String sessionID) throws SQLException {
		String query = "SELECT sessionTime FROM Session WHERE cardID = ? AND sessionID = ?";
		PreparedStatement preparedStmt = connection().prepareStatement(query);
		preparedStmt.setString(1, cardID);
		preparedStmt.setString(2, sessionID);
		ResultSet rs = preparedStmt.executeQuery();
		while (rs.next()) {
			Timestamp sessionTime = rs.getTimestamp("sessionTime");
			Timestamp current = new Timestamp(new Date(System.currentTimeMillis() + 10 * 60 * 1000).getTime());
			if (current.before(sessionTime)) {
				deleteSession(sessionID);
				return false;
			} else {
				deleteSession(sessionID);
				row(cardID, sessionID);
				return true;
			}
		}
		return false;
	}

	public boolean userSesssion(String cardID, String sessionID) throws SQLException {
		String query = "SELECT cardID FROM Session WHERE cardID = ? AND sessionID = ?";
		PreparedStatement preparedStmt = connection()
				.prepareStatement(query);
		preparedStmt.setString(1, cardID);
		preparedStmt.setString(2, sessionID);
		ResultSet rs = preparedStmt.executeQuery();
		while (rs.next()) {
			String cardSessionID = rs.getString("cardID");
			if (cardID.equals(cardSessionID)) {
				deleteSession(sessionID);
				return false;
			}
		}
		row(cardID, sessionID);
		return true;
	}

	public void deleteSession(String sessionID) throws SQLException {
		String query = "DELETE FROM Session WHERE sessionID = ?";
		PreparedStatement preparedStmt = connection().prepareStatement(query);
		preparedStmt.setString(1, sessionID);
		entry(preparedStmt);
	}

}
