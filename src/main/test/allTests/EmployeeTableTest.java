package allTests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import io.swagger.database.EmployeeTable;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeTableTest {

	@Test
	public void validCard() {
		EmployeeTable emp = new EmployeeTable();
		String fullName = emp.employeeCheck("r7jTG7dqBy5wGO4L");
		Assert.assertEquals(fullName, "Daniel Hayward");
	}
	
	@Test
	public void invalidCard() {
		EmployeeTable emp = new EmployeeTable();
		String fullName = emp.employeeCheck("a1qTG7dqBy5wDA0L");
		Assert.assertNotEquals(fullName, "Daniel Hayward");
	}
	
	@Test
	public void validPin() {
		EmployeeTable emp = new EmployeeTable();
		String fcUniqueID = emp.validatePin("1234", "r7jTG7dqBy5wGO4L");
		Assert.assertEquals(fcUniqueID, "PODIY78UZ12");
	}
	
	@Test
	public void invalidPin() {
			EmployeeTable emp = new EmployeeTable();
			String fcUniqueID = emp.validatePin("9999","r7jTG7dqBy5wGO4L");
			Assert.assertNotEquals(fcUniqueID, "PODIY78UZ12");
	}

}
