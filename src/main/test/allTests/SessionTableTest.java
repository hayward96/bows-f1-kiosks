package allTests;

import java.sql.SQLException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import io.swagger.database.SessionTable;
import org.junit.Assert;

@RunWith(MockitoJUnitRunner.class)
public class SessionTableTest {

	static SessionTable session;

	@BeforeClass
	public static void beforeClass() {
		session = new SessionTable();
	}

	@Before
	public void beforeEach() {
		try {
			session.deleteSession("ABCD1234TRE");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void validCreateSession() {
		try {
			boolean sessionCreated = session.row("r7jTG7dqBy5wGO4L", "ABCD1234TRE");
			Assert.assertTrue(sessionCreated);
		} catch (SQLException e) {
			Assert.assertTrue(false);
		}

	}

	@Test
	public void valdiCheckSession() {
		try {
			session.row("r7jTG7dqBy5wGO4L", "ABCD1234TRE");
			boolean sessionValid = session.validSession("r7jTG7dqBy5wGO4L", "ABCD1234TRE");
			Assert.assertTrue(sessionValid);
		} catch (SQLException e) {
			Assert.assertTrue(false);
		}

	}

	@Test
	public void invalidCheckSession() {
		try {
			session.row("r7jTG7dqBy5wGO4L", "ABCD1234TRE");
			boolean sessionValid = session.validSession("r7jTG7dqBy5wGO4L", "ZXYZ1234TRE");
			Assert.assertFalse(sessionValid);
		} catch (SQLException e) {
			Assert.assertTrue(false);
		}
	}

	@Test
	public void checkUserSession() {
		// user double taps, should return false
		try {
			session.row("r7jTG7dqBy5wGO4L", "ABCD1234TRE");
			boolean sessionValid = session.userSesssion("r7jTG7dqBy5wGO4L", "ABCD1234TRE");
			Assert.assertFalse(sessionValid);
		} catch (SQLException e) {
			Assert.assertTrue(false);
		}
	}


	@Test
	public void deleteSession() {
		try {
			session.row("r7jTG7dqBy5wGO4L", "ABCD1234TRE");
			session.deleteSession("ABCD1234TRE");
			Assert.assertTrue(true);
		} catch (SQLException e) {
			Assert.assertTrue(false);
		}

	}

}
